﻿/*
    Main 
    Application start point - sets up extensions and robotlegs
 */

using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Framework.API;
using Robotlegs.Bender.Framework.Impl;
using Robotlegs.Bender.Platforms.Unity.Bundles;
using Robotlegs.Bender.Bundles.MVCS;
using Rancon.Extensions.BootstrapSettings;
using Rancon.Extensions.CallbackTimer.Platforms.Unity;
using Robotlegs.Bender.Platforms.Unity.Extensions.ContextViews.Impl;
using webbstudios.indieassignment.config;

namespace webbstudios.indieassignment
{
    public class Main : MonoBehaviour
    {
        private IContext _context;

        //Entry point of application installing extensions, loading settings and starting app.
        void Start()
        {
            _context = new Context()
                .Install<UnitySingleContextBundle>()
                .Install<MVCSBundle>()
                .Install<UnityCallbackTimerExtension>()
                .Install<UnitySingleContextBundle>()
                .Configure(new TransformContextView(transform))
                .Configure<ApplicationConfig>();
            _context.Initialize();
        }
    }
}
