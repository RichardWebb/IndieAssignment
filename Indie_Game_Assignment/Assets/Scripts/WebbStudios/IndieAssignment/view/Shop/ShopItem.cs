﻿/*
 *  Shop item view,
 *  This class handles the shop items and their price and saving the purchase (In game coins and player prefs only)
 *  Created by Richard Webb, s1308033 
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Security.Cryptography;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view.shop
{
    public class ShopItem : MonoBehaviour
    {
        [SerializeField]
        private float price;

        [SerializeField]
        private int coinPrice = 5000;

        [SerializeField]
        private Button buyBtn;

        [SerializeField]
        private GameObject tick;

        [SerializeField]
        private string name;

        [SerializeField]
        private string key;

        [SerializeField]
        private Button buyWithCoinsBtn;

        [SerializeField]
        private GameObject title;

        [SerializeField]
        private ShopView shop;

        private string path;
        
        void Start()
        {
            //hook inputs.
       //     buyBtn.onClick.AddListener(() => BuyWithMoney());
            buyWithCoinsBtn.onClick.AddListener(() => BuyItemWithCoins());
            GetData();
        }

        private void HideBuyBtns()
        {
            //Toggle buttons off
            buyWithCoinsBtn.gameObject.SetActive(false);
          //  buyBtn.gameObject.SetActive(false);
            title.SetActive(false);
            tick.gameObject.SetActive(true);
        }

        private void BuyItemWithCoins()
        {
            //Check total coins based on price 
            if (shop.totalCoins >= coinPrice)
            {
                shop.SpendCoins(coinPrice);
                SavePurchase();
            }
        }


        private void BuyWithMoney()
        {
            SavePurchase();
        }

        //Grab data from player prefs.
        private void GetData()
        {
            string s = PlayerPrefs.GetString(key);
            if (!string.IsNullOrEmpty(s))
            {
                HideBuyBtns();
            }
        }

        //Encript data
        //http://wiki.unity3d.com/index.php?title=MD5
        private string Md5Sum(string strToEncrypt)
        {
            System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
            byte[] bytes = ue.GetBytes(strToEncrypt);

            // encrypt bytes
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash(bytes);
            
            // Convert the encrypted bytes back to a string (base 16)
            string hashString = "";

            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
            }

            return hashString.PadLeft(32, '0');
        }

        //Save purchase to player prefs
        private void SavePurchase()
        {
            string data = Md5Sum(name);
            PlayerPrefs.SetString(key, data);
            HideBuyBtns();
            GameSettings.GetLatestSettings();
        } 
    }

    [Serializable]
    class Item
    {
        public bool brought;
    }
}
