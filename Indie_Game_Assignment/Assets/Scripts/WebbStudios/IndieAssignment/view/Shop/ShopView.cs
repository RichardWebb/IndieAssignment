﻿/*
 *   ShopView,
 *   This class handles the shop and the contained items and the naviation to the main menu.
 *   Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;
using webbstudios.indieassignment.view.mono;
using UnityEngine.Purchasing;

namespace webbstudios.indieassignment.view.shop
{
    public class ShopView : MonoBehaviour, IStoreListener
    {
        [SerializeField]
        private CanvasGroup cGroup;

        [SerializeField]
        private Button myBtn;

        [SerializeField]
        private MainMenuView mainMenu;

        [SerializeField]
        private Button closeBtn;

        [SerializeField]
        private Text coinTxt;

        public int totalCoins;

        private void Start()
        {
            //Hook input.
            myBtn.onClick.AddListener(() => Show());
            closeBtn.onClick.AddListener(() => Hide());
            totalCoins = PlayerPrefs.GetInt("m_coin_a", 0);
        }

        //Return to main menu.
        private void Hide()
        {
            mainMenu.audioView.PlayClip();

            mainMenu.ShowMenu();
            cGroup.blocksRaycasts = false;
            cGroup.interactable = false;
            cGroup.DOFade(0f, 0.5f);
        }
        //Show shop.
        private void Show()
        {
            mainMenu.audioView.PlayClip();
            mainMenu.HideMenu();
            cGroup.interactable = true;
            cGroup.blocksRaycasts = true;
            coinTxt.text = totalCoins.ToString();
            cGroup.DOFade(1f, 1f);
        }

        //Spend coins and update prefs
        public void SpendCoins(int coinPrice)
        {
            int coins = totalCoins;
            totalCoins -= coinPrice;

            DOTween.To(() => coins, x => coins = x, totalCoins, 1f).SetEase(Ease.Linear).OnUpdate(() =>
            {
                if (coinTxt != null)
                    coinTxt.text = coins.ToString();
            });

            PlayerPrefs.SetInt("m_coin_a", totalCoins);
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("init failed");
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.Log("Init ed");
        }
    }
}


