﻿/*
 * Audio view, used to play sound like jumps, death, button clicks.
 */

using UnityEngine;
using System.Collections;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{

    public class AudioView : MonoBehaviour
    {
        private AudioSource source;

        [SerializeField]
        private bool loop = false;

        [SerializeField]
        private bool playStraightAway = false;

        [SerializeField]
        private AudioClip clip;

        [SerializeField]
        private float vol = 1;

        private void Start()
        {
            source = gameObject.AddComponent<AudioSource>();
            source.loop = loop;

            if (playStraightAway) source.clip = clip;

            if (GameSettings.SoundEnabled)
                source.playOnAwake = playStraightAway;
        }

        //Main play audio clip called from anything that wants audio played.
        public void PlayClip(AudioClip c = null)
        {
            if (GameSettings.SoundEnabled)
                source.PlayOneShot(c == null ? clip : c, vol);
        }
    }
}