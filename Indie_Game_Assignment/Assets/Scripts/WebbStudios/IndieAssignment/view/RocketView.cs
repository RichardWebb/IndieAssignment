﻿/*
 * Rocket power up view.
 * Launch the player up.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using DG.Tweening;
using webbstudios.indieassignment.view.interfaces;
using webbstudios.indieassignment.events;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{
    public class RocketView : EventView, IScoreDispatcher
    {
        [SerializeField]
        private PlayerHealthView healthView;

        [SerializeField]
        private GameObject jumpButton;
        private float powerUpDuration = 5f;

        [SerializeField]
        private GameObject rocket;

        [SerializeField]
        private GameObject particles;

        [SerializeField]
        private GameObject character;

        [SerializeField]
        private PlayerMovementView movementView;

        private float distance = 1000f;

        private int scorevalue = 2500;

        protected override void Start ()
        {
            base.Start();
	    }

        //Toggle power up on.
        public void Activate()
        {
            healthView.isImmune = true;
            movementView.StopMoving();
            TogglePowerUp(true);

            int _mod = GameSettings.rocketModifer;
            //Update players score.
            dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_SCORE, scorevalue * _mod));

            float x = movementView.onLeftSide ?  movementView.leftPillar.transform.position.x : movementView.rightPillar.transform.position.x;

            //Fly rocket!
            character.transform.DOMove(new Vector3(x, character.transform.position.y + (distance * _mod) , 0), powerUpDuration).OnComplete(() => 
            {
                movementView.isJumping = false;

                TogglePowerUp(false);
            });

            StartCoroutine(Disable());
        }

        //Toggle displays.
        private void TogglePowerUp(bool state)
        {
            rocket.SetActive(state);
            particles.SetActive(state);
        }

        //Disable rocket after duration.
        private IEnumerator Disable()
        {
            yield return new WaitForSeconds(powerUpDuration + 1);
            healthView.isImmune = false;
            movementView.Resume();
        }
    }
}
