﻿/*
 * Power up spawn manager.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using System;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{
    public class PowerUpSpawnerView : EventView
    {
        [SerializeField]
        private GameObject[] powerUps;

        [SerializeField]
        private Transform[] spawnPoints;


        private float spawnRate = 7.5f;

	    protected override void Start ()
        {
            base.Start();
            //Start spawning power ups.
            StartCoroutine(SpawnNewPowerUp());
	    }

        //Spawn power up!
        private IEnumerator SpawnNewPowerUp()
        {
            while (true)
            {
                yield return new WaitForSeconds(spawnRate);
                if (GameSettings.GameStarted)
                {
                    GameObject obj = Instantiate(GetRandomPowerUp());
                    obj.transform.SetParent(transform);
                    RectTransform rect = obj.GetComponent<RectTransform>();
                    rect.localScale = new Vector3(1, 1, 1);

                    rect.position = GetRandomPosition();
                    yield return new WaitForSeconds(spawnRate);
                }
            }
        }

        //Get random position for power up
        private Vector3 GetRandomPosition()
        {
            return spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length - 1)].position;
        }

        //Get a random power up to spawn.
        private GameObject GetRandomPowerUp()
        {
            return powerUps[UnityEngine.Random.Range(0, powerUps.Length )];
        }
    }
}
