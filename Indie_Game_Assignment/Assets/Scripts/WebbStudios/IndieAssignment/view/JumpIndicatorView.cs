﻿/*
 * Jump marker, icon displaying where the character will jump to.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using webbstudios.indieassignment.view.interfaces;
using System;
using UnityEngine.UI;
using DG.Tweening;

namespace webbstudios.indieassignment.view
{
    public class JumpIndicatorView : EventView, IInputListener, IPowerUp
    {
        [SerializeField]
        private GameInputView inputView;

      //  [SerializeField]
        private PlayerMovementView movementView;

        [SerializeField]
        private GameObject playerParticles;

        private RectTransform jumpPointRect;

        private float leftPos = -21.87f;

        private float rightPos = 21.58f;

        private Coroutine currentRoutine;

        private Image myImage;
        private bool isInUse = false;

        private LineRenderer lineRenderer;

        [SerializeField]
        private float powerUpDuration = 10;

        //Getting ready.
        void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();
            myImage = GetComponent<Image>();
            StartCoroutine(ToggleImage(false));
        }

        // Use this for initialization
        protected override void Start ()
        {
            base.Start();
            inputView.OnJumpButtonPressed += JumpPressStart;
            jumpPointRect = GetComponent<RectTransform>();
            StartCoroutine(GetPlayer());

        }

        //Get player to indicate for.
        private IEnumerator GetPlayer()
        {
            yield return new WaitForSeconds(0.1f);
            var obj = GameObject.FindGameObjectWithTag("Player");

            movementView = obj.GetComponent<PlayerMovementView>();
            isInUse = true;
            playerParticles = obj.transform.GetChild(0).gameObject;
        }

        //Toggle icon on correct side.
        private void JumpPressStart()
        {
            if (isInUse)
            {
                StartCoroutine(ToggleImage(true));
                lineRenderer.SetPosition(1, transform.position);
                lineRenderer.SetPosition(0, movementView.gameObject.transform.position);
                lineRenderer.enabled = true;
                currentRoutine = StartCoroutine(TrackJump());
            }
        }

        //Track inputs to move the icon where the jump will be going.
        private IEnumerator TrackJump()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();
                lineRenderer.SetPosition(1, transform.position);
                lineRenderer.SetPosition(0, movementView.gameObject.transform.position);
               
                jumpPointRect.position = new Vector3(movementView.onLeftSide ? rightPos : leftPos, movementView.gameObject.transform.position.y + (inputView.currentJumpPower * 2.5f), 0);
            }

        }

        //Reset icon.
        public void JumpPressed(float power)
        {
            if (isInUse)
            {
                StartCoroutine(ToggleImage(false));
                if (currentRoutine != null)
                    StopCoroutine(currentRoutine);
                lineRenderer.SetPosition(0, Vector3.zero);
                lineRenderer.SetPosition(1, Vector3.zero);
                lineRenderer.enabled = false;
            }
        }

        public void MoveWalls()
        {
        }

        //Toggle icon.
        public void Activate()
        {
            isInUse = true;
            playerParticles.SetActive(true);

            StartCoroutine(Disable());
        }

        private IEnumerator Disable()
        {
            yield return new WaitForSeconds(powerUpDuration);
            isInUse = false;
            playerParticles.SetActive(false);

        }

        private IEnumerator ToggleImage(bool state)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            myImage.enabled = state;
        }

        public void Destroy()
        {
            isInUse = false;
        }
    }

}

