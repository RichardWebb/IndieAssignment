﻿/*
 * Main menu view,
 * Entrance for game, links to game, shop and character select from here.
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using DG.Tweening;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using webbstudios.indieassignment.view.mono;


namespace webbstudios.indieassignment.view
{
    public enum Characters
    {
        POP,
        WIFEY_POP,
        GRAMPA_POP,
        PUMPKIN_POP,
        SANTA_POP
    }

    public class MainMenuView : EventView
    {
        [SerializeField]
        private CanvasGroup mainCanvasGroup;

        [SerializeField]
        private CanvasGroup titleCanvasGroup;

        [SerializeField]
        private CanvasGroup buttonsCanvasGroup;

    
        [SerializeField]
        private Button playBtn;

        [SerializeField]
        private GameObject gameView;


        [SerializeField]
        private Characters chosenCharacter = Characters.POP;

        [SerializeField]
        private Sprite[] characterSprites;

        [SerializeField]
        private Image chosenCharacterImage;

        public AudioView audioView;

	    // Use this for initialization
	    protected override void Start ()
        {
          //  PlayerPrefs.DeleteAll();
            base.Start();
           
            Application.targetFrameRate = 60;
            audioView = GetComponent<AudioView>();
            GameSettings.playedAdvert = false;
            //Get all settings.
            GameSettings.GetLatestSettings();

            //hook input.
            playBtn.onClick.AddListener(()=> { PlayBtnPressed(); });

            //Animate menu in.
            titleCanvasGroup.DOFade(1f, 1.5f).SetEase(Ease.OutExpo).OnComplete(() =>
            {
                buttonsCanvasGroup.interactable = true;

                buttonsCanvasGroup.DOFade(1f, 0.75f).SetEase(Ease.OutExpo);
            });

            SelectCharacter(GameSettings.selectedCharacter);

        }

        //Play button pressed -> Load game
        private void PlayBtnPressed()
        {
            audioView.PlayClip();
            titleCanvasGroup.DOFade(0f, 1f);

            buttonsCanvasGroup.interactable = false;
            buttonsCanvasGroup.alpha = 0;

            //Fade menu out for game.
            mainCanvasGroup.DOFade(0f, 1f).OnComplete(()=> 
            {
                GameObject game = Instantiate(gameView);

                game.GetComponent<GameView>().Init(GameSettings.selectedCharacter);
                Destroy(gameObject);
            });
        }

        //Character selection management (Toggles character on main menu)
        public void SelectCharacter(Characters character)
        {
            chosenCharacter = character;
            GameSettings.selectedCharacter = character;
            switch (chosenCharacter)
            {
                case Characters.POP:
                    chosenCharacterImage.sprite = characterSprites[0];
                    break;

                case Characters.WIFEY_POP:
                    chosenCharacterImage.sprite = characterSprites[1];
                    break;

                case Characters.GRAMPA_POP:
                    chosenCharacterImage.sprite = characterSprites[2];
                    break;

                case Characters.PUMPKIN_POP:
                    chosenCharacterImage.sprite = characterSprites[3];
                    break;

                case Characters.SANTA_POP:
                    chosenCharacterImage.sprite = characterSprites[4];
                    break;
            }
        }

        //Hide main menu.
        public void HideMenu()
        {
            titleCanvasGroup.interactable = false;
            buttonsCanvasGroup.interactable = false;

            titleCanvasGroup.DOFade(0f, 0.5f);
            buttonsCanvasGroup.DOFade(0f, 0.5f);
       }

        //Show main menu.
        public void ShowMenu()
        {
            titleCanvasGroup.interactable = true;
            buttonsCanvasGroup.interactable = true;

            titleCanvasGroup.DOFade(1f, 0.5f);
            buttonsCanvasGroup.DOFade(1f, 0.5f);
        }


    }
}
