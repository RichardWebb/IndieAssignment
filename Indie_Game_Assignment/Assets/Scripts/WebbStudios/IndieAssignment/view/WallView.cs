﻿using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using webbstudios.indieassignment.view.interfaces;
using System;
using DG.Tweening;
using webbstudios.indieassignment.view;
using UnityEngine.UI;
using webbstudios.indieassignment.view.mono;
namespace webbstudio.indieassignment.view
{
    public class WallView : EventView, IInputListener
    {
        private RectTransform myRect;
        public Material myMat;

        public void JumpPressed(float power){}
        private float currentOffset = 0;
        private bool stopChecking = false;

        private WallSpawner spawner;


        public void Init(WallSpawner spawner)
        {
            this.spawner = spawner;
        }

        // Use this for initialization
        protected override void Start ()
        {
            //hook mediators.
            base.Start();
	    }

        void Update()
        {
            if (stopChecking) return;
            //Check position for recycle.
            if (transform.localPosition.y + 3000f < Camera.main.transform.localPosition.y)
            {
                stopChecking = true;
                Recycle();
            }
        }

        //Recycle the wall.
        private void Recycle()
        {
            GetComponent<RectTransform>().anchoredPosition = spawner.Recycle();
            stopChecking = false;
        }

        public void MoveWalls()
        {
        }

    }
}

