﻿/*
 * Audio manager view - 
 * Handles toggle buttons on main menu and saves audio settings.
 * Created by Richard Webb.
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace webbstudios.indieassignment.view.mono
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField]
        private Button soundBtn;

        [SerializeField]
        private Button musicBtn;

        [SerializeField]
        private Image musicImage;

        [SerializeField]
        private Image soundImage;

        [SerializeField]
        private Sprite[] soundImages;

        [SerializeField]
        private Sprite[] musicImages;

        [SerializeField]
        private AudioSource source;

        void Start ()
        {
            //Get latest settings.
            var musicEnabled = PlayerPrefs.GetString("MusicEnabled");
            if (string.IsNullOrEmpty(musicEnabled))
            {
                GameSettings.MusicEnabled = true;
            }
            else
            {
                GameSettings.MusicEnabled = bool.Parse(musicEnabled);
            }

            var soundEnabled = PlayerPrefs.GetString("SoundEnabled");
            if (string.IsNullOrEmpty(musicEnabled))
            {
                GameSettings.SoundEnabled = true;
            }
            else
            {
                GameSettings.SoundEnabled = bool.Parse(soundEnabled);
            }

            musicImage.sprite = musicImages[GameSettings.MusicEnabled ? 0 : 1];
            soundImage.sprite = soundImages[GameSettings.SoundEnabled ? 0 : 1];

            source.mute = !GameSettings.MusicEnabled;

            //Button click hookings
            soundBtn.onClick.AddListener(() => 
            {
                GameSettings.SoundEnabled = !GameSettings.SoundEnabled;
                PlayerPrefs.SetString("SoundEnabled", GameSettings.SoundEnabled.ToString());
                soundImage.sprite = soundImages[GameSettings.SoundEnabled ? 0 : 1];

            });

            musicBtn.onClick.AddListener(() =>
            {
                GameSettings.MusicEnabled = !GameSettings.MusicEnabled;
                PlayerPrefs.SetString("MusicEnabled", GameSettings.MusicEnabled.ToString());
                musicImage.sprite = musicImages[GameSettings.MusicEnabled ? 0 : 1];
                source.mute = !GameSettings.MusicEnabled;

            });
        }
	
	    // Update is called once per frame
	    void Update () {
	
	    }
    }
}
