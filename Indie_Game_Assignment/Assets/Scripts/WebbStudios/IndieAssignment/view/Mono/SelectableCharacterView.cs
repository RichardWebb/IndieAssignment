﻿/*
 * Toggle character in selection view
 * Created by Richard Webb, s1308033 
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace webbstudios.indieassignment.view.mono
{
    public class SelectableCharacterView : MonoBehaviour
    {
        [SerializeField]
        private string key;

        [SerializeField]
        private Button myBtn;

	    void Start ()
        {
            //PlayerPrefs.DeleteAll();
        }

        public void Init()
        {
            //Enabled / dis enable character if we have brought it.
            string s = PlayerPrefs.GetString(key);
            myBtn.gameObject.SetActive(!string.IsNullOrEmpty(s));
        }
    }

}
