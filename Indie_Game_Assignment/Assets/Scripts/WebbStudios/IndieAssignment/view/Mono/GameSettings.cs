﻿/*
 * Game settings view.
 * General settings of the game. 
 * Contains sound, music and power up settings.
 * Created by Richard Webb, s1308033 
 */
using UnityEngine;
using System.Collections;

namespace webbstudios.indieassignment.view.mono
{
    public static class GameSettings 
    {
        public static int rocketModifer = 1;
        public static int doubleScoreModifer = 2;
        public static int helmetModifer = 1;

        public static Characters selectedCharacter = Characters.POP;

        public static void GetLatestSettings()
        {
            string r = PlayerPrefs.GetString("66#66#25");
            string d = PlayerPrefs.GetString("45#85#22");
            string h = PlayerPrefs.GetString("58#59#52");

            rocketModifer = string.IsNullOrEmpty(r) ? 1 : 2;
            doubleScoreModifer = string.IsNullOrEmpty(d) ? 2 : 4;
            helmetModifer = string.IsNullOrEmpty(h) ? 1 : 2;
        }

        public static bool GameStarted = false;
        public static bool SoundEnabled = false;
        public static bool MusicEnabled = false;
        public static bool playedAdvert = false;
    }
}
