﻿/*
 *  ObstacleMoveView 
 *  This class manages the movement of obstacles it will move items left and right or up and down.
 *  Created by Richard Webb, s1308303
 */

using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace webbstudios.indieassignment.view.mono
{
    public class ObstacleMoveView : MonoBehaviour
    {
        public enum SlideStates
        {
            UP_DOWN,
            LEFT_RIGHT
        }

        [SerializeField]
        private SlideStates slideDirection;

        [SerializeField]
        private Ease easing = Ease.Linear;

        [SerializeField]
        private float duration = 0.5f;

        [SerializeField]
        private float distance = 500;
            
        [SerializeField]
        private float delay = 0;

        [SerializeField]
        private bool randomiseDelay = false;


        private Vector3 startPos = Vector3.zero;
        private Vector3 endPos = Vector3.zero;

        void Start ()
        {
            Vector3 _startPos = transform.localPosition;
            //Get random movement delay.
            if (randomiseDelay)
            {
                delay = UnityEngine.Random.Range( (delay > 0 ? delay * 0.5f : 0), (delay > 0 ? delay + (delay * 1.5f) : 1) );
            }

            //get positions to move between.
            switch (slideDirection)
            {
                case SlideStates.LEFT_RIGHT:
                    startPos = _startPos;
                    endPos = _startPos.x < 0 ? new Vector3(_startPos.x - distance, _startPos.y, _startPos.z) : new Vector3(_startPos.x + distance, _startPos.y, _startPos.z);
                    break;

                case SlideStates.UP_DOWN:
                    startPos = _startPos;
                    endPos = new Vector3(_startPos.x, _startPos.y - distance, _startPos.z);
                    break;
            }

            DOObstacleMove();
        }

        //Move obstacle between points
        private void DOObstacleMove (bool goToEnd = true)
        {
            transform.DOLocalMove(goToEnd ? endPos : startPos, duration).SetEase(easing).SetDelay(delay).OnComplete(()=> 
            {
                DOObstacleMove(!goToEnd);
            });
        }
    }
}
