﻿/*
 *  AdditionalItemsView 
 *  Extra items to be toggled based on chosen character.
 *  Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;

namespace webbstudios.indieassignment.view.mono
{
    public class AdditionalItemsView : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] objs;

        void Start ()
        {
            foreach (GameObject obj in objs)
            {
                obj.SetActive(true);
            }
	    }
    }
}
