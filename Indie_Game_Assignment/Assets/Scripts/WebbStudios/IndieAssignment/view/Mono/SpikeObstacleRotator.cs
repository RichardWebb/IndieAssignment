﻿/*
 *  SpikeObstacleRotator - Simple rotator for spike obstacle.
 *  Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

namespace webbstudios.indieassignment.view.mono
{
    public class SpikeObstacleRotator : MonoBehaviour
    {
        private float rotationLeftZ = -45f;
        private float rotationRightZ = 45f;

        private Vector3 leftRot;
        private Vector3 rightRot;

        // Use this for initialization
        void Start ()
        {
            leftRot = new Vector3(0, 0, rotationLeftZ);
            rightRot = new Vector3(0, 0, rotationRightZ);
            DoRotation(true);
	    }

        private void DoRotation(bool left)
        {
            transform.DOLocalRotate(left ? leftRot : rightRot, 1.5f).SetEase(Ease.Linear).OnComplete(()=> 
            {
                DoRotation(!left);
            });
        }
    }
}

