﻿/*
 * ItemRotator - 
 * Basic item rotator
 * Created by Richard Webb.
*/

using UnityEngine;
using System.Collections;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignemnt.view.mono
{
    public class ItemRotator : MonoBehaviour
    {
        [SerializeField]
        private Vector3 direction;

        [SerializeField]
        private float speed = 5;

        void FixedUpdate ()
        {
            if (GameSettings.GameStarted)
              transform.Rotate(direction * speed);
	    }
    }

}
