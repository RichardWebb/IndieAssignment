﻿/*
 *  Camera follow - Used for the camera to follow the player after jumping
 *  Created by Richard Webb, s1308033 
 */
using UnityEngine;
using System.Collections;
using webbstudios.indieassignment.view.interfaces;
using System;
using DG.Tweening;

public class CameraFollow : MonoBehaviour, IInputListener
{
    private Transform target;
    private Vector3 velocity = Vector3.zero;
    private Vector3 offset;

    private bool isMoving = false;
    private float lastYPos;

    void Start ()
    {
       
    }

    void LateUpdate()
    {
        if (target != null)
        {
            //Move to target if target has jumped.
            isMoving = target.position.y + 15 > lastYPos + 15;
            if (isMoving)
            {
                lastYPos = target.position.y;
                transform.position = new Vector3(transform.position.x, lastYPos, transform.position.z);
            }
        }
    }

    public void SetTarget(Transform _target )
    {
        target = _target;
        offset = transform.position - target.position;

    }

    public void DOCameraShake()
    {
       
    }

    public void JumpPressed(float power)
    {
    }


    public void MoveWalls()
    {
    }
}

