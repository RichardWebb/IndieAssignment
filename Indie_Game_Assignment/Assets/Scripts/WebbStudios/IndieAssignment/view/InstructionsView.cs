﻿/*
 * Instructions view,
 * overlay panel displaying the controls for the game play.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{
    public class InstructionsView : MonoBehaviour
    {
        [SerializeField]
        private Button myBtn;

        void Start()
        {
            GameSettings.GameStarted = false;
            //Fade in
            GetComponent<CanvasGroup>().DOFade(1f, 0.25f).SetEase(Ease.Linear).SetDelay(0.2f).OnComplete(() => 
            {
                myBtn.onClick.AddListener(() =>
                {
                    GameSettings.GameStarted = true;
                    Destroy(this.gameObject);
                });
            });
        }
    }
}