﻿/*
 * user input interface.
 * Created by Richard Webb, s1308033 
*/

namespace webbstudios.indieassignment.view.interfaces
{
    public interface IInputListener
    {
        void JumpPressed(float power);
        void MoveWalls();
    }
}
