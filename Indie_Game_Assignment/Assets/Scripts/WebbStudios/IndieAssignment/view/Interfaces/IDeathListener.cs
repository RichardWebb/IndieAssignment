﻿/*
 * Player death interface.
 * Created by Richard Webb, s1308033 
*/

namespace webbstudios.indieassignment.view.interfaces
{
    public interface IDeathListener
    {
        void PlayerDied();
    }
}