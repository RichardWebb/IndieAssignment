﻿/*
 * Power up interface.
 * Created by Richard Webb, s1308033 
*/
namespace webbstudios.indieassignment.view.interfaces
{
    public interface IPowerUp
    {
        void Destroy();
    }
}
