﻿/*
    Death screen view
    Overlay panel for the end of game 
    Displays score and coins.
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.Advertisements;
using System;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{
    public class DeathScreenView : MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup deathScreen;

        [SerializeField]
        private Text scoreText;

        [SerializeField]
        private Button myBtn;

        [SerializeField]
        private Text coinsText;

        private ScoreView scoreView;

        // Use this for initialization
        void Start()
        {
            //hook button input.
            myBtn.onClick.AddListener(() =>
            {
                SceneManager.LoadScene(0);
            });

            scoreView = GameObject.FindObjectOfType<ScoreView>();

        }

        //Toggle death screen on.
        public void ShowDeathScreen()
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = HandleEndGame;
            //Show advert
            if (!GameSettings.playedAdvert)
            {
                //Cheap fix. Multiple advert infinite loop (03/01/17 RW)
                GameSettings.playedAdvert = true;

                Advertisement.Show(null, options);        
            }
        }

        //Display score and coins - animated
        private void HandleEndGame(ShowResult showResult)
        {
            deathScreen.blocksRaycasts = true;
            deathScreen.interactable = true;

            int totalCoins = PlayerPrefs.GetInt("m_coin_a", 0);
            int c = 0;
            int earnedCoins = WorkOutCoins(scoreView.currentScore);

            PlayerPrefs.SetInt("m_coin_a", totalCoins + earnedCoins);
            DOTween.To(() => c, x => c = x, earnedCoins, 1f).SetEase(Ease.Linear).OnUpdate(() =>
            {
                if (coinsText != null)
                    coinsText.text = c.ToString();
            });

            scoreText.text = scoreView.currentScore.ToString();
            deathScreen.DOFade(1f, 0.5f);
        }

        //Get coins earned based on score.
        private int WorkOutCoins(int currentScore)
        {
            int result = 0;
            if (currentScore > 0)
            {
                result = Mathf.RoundToInt(currentScore * 0.01f);
            }

            return result;
        }
    }
}