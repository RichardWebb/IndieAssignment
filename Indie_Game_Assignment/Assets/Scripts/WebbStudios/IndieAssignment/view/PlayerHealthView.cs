﻿/*
 * Player health view
 * Handles players health and getting hit by an obstacle.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using UnityEngine.SceneManagement;
using System;
using DG.Tweening;
using UnityEngine.UI;
using webbstudios.indieassignment.view.interfaces;
using webbstudios.indieassignment.events;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{
    public class PlayerHealthView : EventView, IDeathListener
    {
        [SerializeField]
        private DeathScreenView deathScreen;

        [SerializeField]
        private AudioClip clip;

        public bool isImmune = false;
        public bool hasDied = false;

        // Use this for initialization
	    protected override void Start ()
        {
            base.Start();
	    }

        //Check collision with obstacles.
        void OnTriggerEnter2D(Collider2D col)
        {
            if (hasDied) return;

            if (isImmune)
            {
                ObstacleView view = col.gameObject.GetComponent<ObstacleView>();
                if (view != null) view.DestroyFromHit();
                return;
            }

            if (col.gameObject.tag == "Obstacle")
            {
                hasDied = true;
                dispatcher.Dispatch(new DeathEvent(DeathEvent.Type.PLAYER_DIED));
            }
        }

        private void PlayDeath()
        {
            
            
        }
        //Handle player died. 
        public void PlayerDied()
        {
            PlayerMovementView view = GetComponent<PlayerMovementView>();
            //Stop jumping.
            view.currentJumpTween.Kill();
            view.trailRenderer.enabled = true;
            view.enabled = false;

            //disable colliders.
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<AudioView>().PlayClip(clip);
            GameSettings.GameStarted = false;

            //Animate off screen.
            transform.DOJump(new Vector3(view.onLeftSide ? view.rightPillar.transform.position.x : view.leftPillar.transform.position.x, transform.position.y - 100), 25, 1, 1.2f).OnComplete(() =>
            {
                deathScreen.ShowDeathScreen();
            });
        }
    }
}
