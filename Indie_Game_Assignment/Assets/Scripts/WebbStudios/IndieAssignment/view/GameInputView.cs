﻿/*
 * Game input view - jump button press and hold management.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using UnityEngine.UI;
using System;
using WebbStudios.IndieAssignment.events;
using webbstudios.indieassignment.view.interfaces;
using UnityEngine.EventSystems;

namespace webbstudios.indieassignment.view
{
    public class GameInputView : EventView, IInputListener, IPointerDownHandler, IPointerUpHandler, IDeathListener
    {
        [SerializeField]
        private Button myButton;

        public float currentJumpPower;

        private bool isbuttonHeld = false;

        [SerializeField]
        private float minJumpPower = 10;

        [SerializeField]
        private float maxJumpPower = 50;

        [SerializeField]
        private float powerIncrements = 5f;

        [SerializeField]
        private Slider powerSlider;

        [SerializeField]
        private Image sliderImage;

        private PlayerMovementView movementView;

        public delegate void JumpListener();
        public JumpListener OnJumpButtonPressed;

        private Color startingColor;
        private bool isJumping;
        private bool isIncreasing = true;

        public void MoveWalls()
        {
        }

        //setup display for input.
        protected override void Start ()
        {
            base.Start();
            startingColor = sliderImage.color;
            powerSlider.minValue = minJumpPower;
            powerSlider.maxValue = maxJumpPower;
            powerSlider.value = minJumpPower;
            movementView = GameObject.FindObjectOfType<PlayerMovementView>();
	    }

        //Power event dispatch management.
        private void SendPowerEvent()
        {
            if (!movementView.isJumping)
            {
                dispatcher.Dispatch(new InputEvent(InputEvent.Type.JUMP, currentJumpPower));
                //Reset display.
                sliderImage.color = startingColor;
                currentJumpPower = minJumpPower;
                powerSlider.value = currentJumpPower;
            }
        }

        //Button hold management.
        public void OnPointerDown(PointerEventData eventData)
        {
            isbuttonHeld = true;
            if (OnJumpButtonPressed != null) OnJumpButtonPressed();
            isIncreasing = true;
            StartCoroutine(StartPowerTimer());
        }

        //Button up management.
        public void OnPointerUp(PointerEventData eventData)
        {
            SendPowerEvent();
            isbuttonHeld = false;
        }


        //While button is held -> do this.
        private IEnumerator StartPowerTimer()
        {
            while (isbuttonHeld)
            {
                yield return new WaitForEndOfFrame();
                if (currentJumpPower <= maxJumpPower)
                {
                    if (!movementView.isJumping)
                    {
                        float power = currentJumpPower + powerIncrements;

                        if (power < minJumpPower) power = minJumpPower;
                        //work out power
                        if (isIncreasing)
                            currentJumpPower =  power <= maxJumpPower ?  power + powerIncrements : maxJumpPower;
                        else
                            currentJumpPower = power > minJumpPower ? currentJumpPower - powerIncrements : minJumpPower;

                        currentJumpPower = Mathf.Clamp(currentJumpPower, minJumpPower, maxJumpPower);

                        powerSlider.value = currentJumpPower;
                        if (powerSlider.value >= maxJumpPower * 0.75f)
                        {
                            sliderImage.color = Color.red;
                        }

                        if (currentJumpPower >= maxJumpPower-0.1f) isIncreasing = false;
                        if (currentJumpPower <= minJumpPower+0.1f) isIncreasing = true;

                    }
                }
          
            }

            SendPowerEvent();
        }

        public void JumpPressed(float power)
        {
        }

        public void PlayerDied()
        {
            Destroy(this);
        }
    }
}
