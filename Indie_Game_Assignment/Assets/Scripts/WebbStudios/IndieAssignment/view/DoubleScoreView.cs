﻿/*
 * Double score power up view
 * Doubles the users score from jumping or using the rocket.
 */
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{
    public class DoubleScoreView : MonoBehaviour
    {
        [SerializeField]
        private GameObject timesTwoText;

        [SerializeField]
        private ScoreView scoreView;

        [SerializeField]
        private Color inUseColor;

        [SerializeField]
        private Text scoreText;

        private float duration = 7.5f;
        private Coroutine currentRoutine;
        private int modifier = 2;

        //Toggle power up on.
        public void Activate()
        {
            if (currentRoutine != null) StopCoroutine(currentRoutine);
            currentRoutine = StartCoroutine(DoPowerUp());
            //Get current modifier
            modifier =  GameSettings.doubleScoreModifer;
            timesTwoText.GetComponent<Text>().text = "X" + (modifier).ToString();
        }

        //Run power up and disable when finished.
        private IEnumerator DoPowerUp()
        {
            scoreView.UpdateScoreMod(modifier);
            scoreText.color = inUseColor;
            timesTwoText.SetActive(true);

            yield return new WaitForSeconds(duration * modifier );

            timesTwoText.SetActive(false);
            scoreText.color = Color.red;
            scoreView.UpdateScoreMod(1);
        }

    }
}
