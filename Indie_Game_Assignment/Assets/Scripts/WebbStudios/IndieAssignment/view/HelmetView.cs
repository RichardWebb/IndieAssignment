﻿/*
 * Helmet power up view 
 * Activated once the player hits the helmet object. 
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using System;
using webbstudios.indieassignment.view.mono;
using UnityEngine.UI;
using DG.Tweening;

namespace webbstudios.indieassignment.view
{
    public class HelmetView : EventView
    {
        [SerializeField]
        private PlayerHealthView healthView;

        [SerializeField]
        private GameObject particles;

        [SerializeField]
        private GameObject helmet;

        [SerializeField]
        private Image helmetImage;

        private float powerUpDuration = 5f;
        private Coroutine currentRoutine;

        protected override void Start ()
        {
            base.Start();
	    }

        //Toggle the power up on and reset the cooldown if already in use.
        public void Activate()
        {
            TogglePowerUp(true);
            helmetImage.DOFade(1f, 0.1f);
            powerUpDuration = powerUpDuration * GameSettings.helmetModifer;
            if (currentRoutine != null) StopCoroutine(currentRoutine);
            currentRoutine = StartCoroutine(Disable());
        }

        //Toggle power up display items 
        private void TogglePowerUp(bool state)
        {
            helmetImage.gameObject.SetActive(state);
            helmet.SetActive(state);
            healthView.isImmune = state;
            particles.SetActive(state);
        }

        //Disable power up after duration use.
        private IEnumerator Disable()
        {
            helmetImage.DOFade(0f, powerUpDuration+2.5f);
            yield return new WaitForSeconds(powerUpDuration);
            TogglePowerUp(false);

        }
    }
}
