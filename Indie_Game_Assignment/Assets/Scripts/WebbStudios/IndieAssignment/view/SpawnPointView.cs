﻿/*
    Spawn point view, handles locking a spawn point for the obstacles.
    Created by Richard Webb, s1308033.
 */
using UnityEngine;
using System.Collections;

namespace webbstudios.indieassignment.view
{
    public class SpawnPointView : MonoBehaviour
    {
        private bool _isOccupied = false;
        public bool isOccupied
        {
            get
            {
                return _isOccupied;
            }
        }

        private ObstacleSpawnerView spawner;

        public void OccupySpawnPoint(ObstacleSpawnerView _spawner)
        {
            if (_spawner != null) spawner = _spawner;

            _isOccupied = true;
        }

        //Release spawn point.
        public void FreeSpawnPoint()
        {
            _isOccupied = false;
            spawner.AddFreePoint(this.gameObject);

        }



    }
}
