﻿/*
 *  CharacterSelectionView, handles the selection of characters in the character select screen
 *  Created By Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using UnityEngine.UI;
using System;
using DG.Tweening;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{
    public class CharacterSelectionView : EventView
    {
        [SerializeField]
        private Button chooseACharacterBtn;

        [SerializeField]
        private CanvasGroup characterSelectionCGroup;

        [SerializeField]
        private CanvasGroup[] unwantedCanvasGroups;

        [SerializeField]
        private ScrollRect scrollRect;

        [SerializeField]
        private MainMenuView mainView;

        [SerializeField]
        private SelectableCharacterView[] characters;

        [SerializeField]
        private Button closeBtn;

        [SerializeField]
        private CanvasGroup shop;

	    protected override void Start ()
        {
            base.Start();
            //Hook inputs.
            chooseACharacterBtn.onClick.AddListener(() => ToggleCharacterSelect());
            closeBtn.onClick.AddListener(() => CloseCharacterSelect());
	    }

        //Close the selection window - return to the main menu.
        private void CloseCharacterSelect()
        {
            mainView.audioView.PlayClip();

            mainView.ShowMenu();
            scrollRect.enabled = false;
            characterSelectionCGroup.interactable = false;

            foreach (CanvasGroup cGroup in unwantedCanvasGroups)
            {
                cGroup.blocksRaycasts = true;
            }

            characterSelectionCGroup.DOFade(0f, 0.5f);
        }

        //Show character select screen.
        private void ToggleCharacterSelect()
        {
            mainView.audioView.PlayClip();

            foreach (SelectableCharacterView character in characters)
            {
                character.Init();
            }

            shop.blocksRaycasts = false;
            foreach (CanvasGroup cGroup in unwantedCanvasGroups)
            {
                cGroup.interactable = false;
                cGroup.alpha = 0;
                cGroup.blocksRaycasts = false;
            }

            characterSelectionCGroup.interactable = true;
            mainView.HideMenu();
            characterSelectionCGroup.DOFade(1f, 0.75f).SetEase(Ease.Linear).OnComplete(()=> 
            {
                scrollRect.enabled = true;
            });
        }

        //Select a character - return to the main menu
        public void SelectCharacter(Characters character)
        {
            mainView.audioView.PlayClip();

            mainView.SelectCharacter(character);

            characterSelectionCGroup.interactable = false;
            characterSelectionCGroup.alpha = 0;


            foreach (CanvasGroup cGroup in unwantedCanvasGroups)
            {
                cGroup.interactable = true;
                cGroup.alpha = 1;
                cGroup.blocksRaycasts = true;
            }

        }
    }
}
