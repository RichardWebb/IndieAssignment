﻿/*
 * Score view manager
 * Handles game score.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using webbstudios.indieassignment.view.interfaces;
using System;
using UnityEngine.UI;
using DG.Tweening;

namespace webbstudios.indieassignment.view
{
    public class ScoreView : EventView, IInputListener, IScoreListener
    {
        public int currentScore;

        [SerializeField]
        private Text scoreText;

        private int scoreModifier = 1;

        protected override void Start ()
        {
            base.Start();
	    }

        //Handle jump pressed 
        public void JumpPressed(float power)
        {
            UpdateScore(power);
        }

        public void UpdateScoreMod(int value)
        {
            scoreModifier = value;
        }

        //Update current game score.
        private void UpdateScore(float power)
        {
            int jumpScore = (int)(5 * power)*scoreModifier;
            int result = currentScore + jumpScore;
            TweenScore(currentScore, result, 0.5f);
            currentScore = result;
        }

        public void MoveWalls()
        {
        }

        //Animate score display.
        private void TweenScore(int start, int value, float duration)
        {
            int _value = start;
            DOTween.To(() => _value, x => _value = x, value, duration).SetEase(Ease.Linear).OnUpdate(()=> 
            {
                if (scoreText != null)
                    scoreText.text = _value.ToString();
            });
        }

        public void UpdateScore(int value)
        {
            TweenScore(currentScore, currentScore + value,  5f);
            currentScore += value;

        }
    }
}
