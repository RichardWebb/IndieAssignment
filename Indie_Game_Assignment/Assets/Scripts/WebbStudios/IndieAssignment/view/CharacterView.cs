﻿/*
    Character view, input view hooks back to the selection view.
    Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace webbstudios.indieassignment.view
{
    public class CharacterView : MonoBehaviour
    {
        [SerializeField]
        private CharacterSelectionView selectionView;

        [SerializeField]
        private Characters character;

        [SerializeField]
        private Button myBtn;

        void Start()
        {
            //hook button.
            myBtn.onClick.AddListener(() =>
            {
                selectionView.SelectCharacter(character);
            });
        }
    }
}