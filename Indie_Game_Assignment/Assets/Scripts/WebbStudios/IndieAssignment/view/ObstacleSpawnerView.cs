﻿/*
 * Obstacle spawning management.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using webbstudios.indieassignment.constant;

namespace webbstudios.indieassignment.view
{
    public class ObstacleSpawnerView : MonoBehaviour
    {
        [SerializeField]
        private Transform[] spawnPoints;

        private List<GameObject> freeSpawnSpoints = new List<GameObject>();

        [SerializeField]
        private Transform parent;

        [SerializeField]
        private GameObject[] obstacleObjs;

        private int maxObstacles = 1;

        [SerializeField]
        private Transform[] _spawnPoints;

        [SerializeField]
        private ScoreView scoreView;

        private float currentY = 0;

        private int maxSpawn = 50;
        [SerializeField]
        private int currentSpawnCount = 0;
        private float maxDistance = 100;
        private float minDistance = 20;

        private float lastScore = 0;

        // Use this for initialization
        void Start ()
        {
            //Get all spawn points.
            foreach (Transform spawnPoint in spawnPoints)
            {
                freeSpawnSpoints.Add(spawnPoint.gameObject);
            }
            currentY = _spawnPoints[0].position.y + 150;

            StartCoroutine(CheckScore());
            //Create all the objecstm
            foreach (GameObject obj in obstacleObjs)
            {
                obj.CreatePool(100);
            }
            Init();
	    }

        private IEnumerator CheckScore()
        {
            while (true)
            {
                yield return new WaitForSeconds(5);
                if (scoreView.currentScore > 7000)
                {
                    if (lastScore == 0) lastScore = scoreView.currentScore;
                    else
                    {
                        //increase difficulty based on score            
                        var s = scoreView.currentScore - lastScore;
                        if (s >= 1000)
                        {
                            lastScore = 0;
                            maxDistance -= 5;
                            maxDistance = Mathf.Clamp(maxDistance, 40, 100);
                        }
                    }

                }
            }
        }

        private void Init()
        {
            //Pre - populate starting obstacles 
            for (int i = 0; i < maxSpawn; i++)
            {
                if (i == 10) maxObstacles++;
                if (i == 20) maxObstacles++;
                SpawnObstacle();
            }

            maxObstacles = 2;

            StartCoroutine(SpawnObstacles());
            StartCoroutine(IncreaseDifficulty());
        }

        public void FreeSpawnPoint()
        {
            currentSpawnCount--;
        }

        private IEnumerator SpawnObstacles()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.1f);
            //Check if new obstacle needed and spawn if so
                if (currentSpawnCount < maxSpawn)
                {
                    SpawnObstacle();
                }
            }
        }

        //Spawn single obstacle.
        private void SpawnObstacle()
        {
            currentSpawnCount++;
            //GameObject obj = Instantiate(GetRandomObstacle());
            GameObject obj = GetRandomObstacle().Spawn();
            CanvasGroup c = obj.GetComponent<CanvasGroup>();
            obj.GetComponent<CanvasGroup>().alpha = 0;
            obj.transform.SetParent(parent);
            obj.GetComponent<RectTransform>().localScale = new Vector2(1, 1);

            obj.transform.position = GetRandomPoint();
            obj.GetComponent<ObstacleView>().Init(this);
            
            StartCoroutine(FadeObjectIn(obj));

            currentY += GetRandomDistance();
        }

        //Small delay of item fade in
        private IEnumerator FadeObjectIn(GameObject obj)
        {
            yield return new WaitForSeconds(1);
            obj.GetComponent<CanvasGroup>().alpha = 1;

        }

        //Return random spawn point
        private Vector3 GetRandomPoint()
        {
            Transform trans = _spawnPoints[UnityEngine.Random.Range(0, _spawnPoints.Length)];
            return new Vector3(trans.position.x, currentY, trans.position.z);
        }

        //Get random distance for obstacle
        private float GetRandomDistance()
        {
            return UnityEngine.Random.Range(minDistance, maxDistance);
        }

        //Increase obstacle range (soft difficulty change)
        private IEnumerator IncreaseDifficulty()
        {
            int max = obstacleObjs.Length - 1;
            while (maxObstacles < max)
            {
                yield return new WaitForSeconds(15);
                maxObstacles++;
            }
        }

      /*  private IEnumerator SpawnNewObstacle()
        {
            while (true)
            {            
                yield return new WaitForSeconds(1f);
                if (freeSpawnSpoints.Count > 0)
                {
                    GameObject obj = Instantiate(GetRandomObstacle());
                    obj.transform.SetParent(parent);
                    obj.GetComponent<RectTransform>().localScale = new Vector2(1, 1);

                    GameObject spawnPoint = GetSpawnPoint();
                    SpawnPointView view = spawnPoint.GetComponent<SpawnPointView>();
                    view.OccupySpawnPoint(this);
                 //   obj.GetComponent<ObstacleView>().Init(view);
                    obj.transform.position = spawnPoint.transform.position;
                    freeSpawnSpoints.Remove(spawnPoint);
                }
            }
        }*/

        //Get a random obstacle to spawn.
        private GameObject GetRandomObstacle()
        {
            int randomIndex = UnityEngine.Random.Range(0, Mathf.Clamp(maxObstacles, 1, obstacleObjs.Length - 1));

            return obstacleObjs[randomIndex];
        }

        //Spawn point management.
        public void AddFreePoint(GameObject obj)
        {
            freeSpawnSpoints.Add(obj);
        }
        
        private GameObject GetSpawnPoint()
        {
            int randomPoint = UnityEngine.Random.Range(0, freeSpawnSpoints.Count - 1);

            return freeSpawnSpoints[randomPoint];

        }
    }
}
