﻿/*
 * Wall spawning manager.
 * Managed wall spawning and wrapping around.
 * Created by Richard Webb, s138033
 */
using UnityEngine;
using System.Collections;
using webbstudios.indieassignment.view.mono;
using System;
using webbstudio.indieassignment.view;
using UnityEngine.UI;

namespace webbstudios.indieassignment.view
{
    public class WallSpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] walls;

        [SerializeField]
        private Transform startPoint;

        private float currentY = 0;
        private float yIncrements = 0;

        private GameObject wall;
        private int startingWallsCount = 5;

        void Start ()
        {
            //Change theme based on character selected.
            switch (GameSettings.selectedCharacter)
            {
                case Characters.SANTA_POP:
                    wall = walls[1];
                    break;

                case Characters.PUMPKIN_POP:
                    wall = walls[2];
                    break;

                default:
                    wall = walls[0];
                    break;
            }
            for (int i = 0; i < startingWallsCount; i++)
            {
                SpawnWall();
            }
        }
	
	    void Update ()
        {
	
	    }

        //Reposition wall if off screen.
        public Vector2 Recycle()
        {
            Vector2 v2  = new Vector2(startPoint.transform.localPosition.x, currentY);
            currentY = currentY + yIncrements;
            return v2;
        }

        //Create wall 
        private void SpawnWall()
        {
            GameObject obj = Instantiate(wall);
            obj.transform.SetParent(transform);
            obj.GetComponent<RectTransform>().localScale = Vector3.one;
            if (yIncrements == 0)
                yIncrements = obj.GetComponent<RectTransform>().rect.height;

            obj.GetComponent<RectTransform>().anchoredPosition= new Vector2(startPoint.transform.localPosition.x, currentY);
            currentY = currentY + yIncrements;


            obj.GetComponent<WallView>().Init(this);
        }
    }
}
