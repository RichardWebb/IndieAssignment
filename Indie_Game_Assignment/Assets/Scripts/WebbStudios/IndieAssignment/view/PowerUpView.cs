﻿/*
 * Power up states class. 
 * Created by Richard Webb, s1308033 
 */
using UnityEngine;
using System.Collections;

namespace webbstudios.indieassignment.view
{
    public class PowerUpView : MonoBehaviour
    {
        public enum PowerUp
        {
            ROCKET,
            JUMP_AID, 
            HELMET,
            TIMES_2_MODIFER
        }

        public PowerUp myPowerUp;
    }
}