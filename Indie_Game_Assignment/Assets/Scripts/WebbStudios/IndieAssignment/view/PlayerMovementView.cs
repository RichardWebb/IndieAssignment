﻿/*
 * Player movement view
 * handles movement of the pop - listens for input from input manager.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using DG.Tweening;
using UnityEngine.UI;
using webbstudios.indieassignment.view.interfaces;
using System;
using WebbStudios.IndieAssignment.events;
using UnityEngine.SceneManagement;
using webbstudios.indieassignment.view.mono;

namespace webbstudios.indieassignment.view
{
    public class PlayerMovementView : EventView, IInputListener
    {
        [SerializeField]
        public RectTransform leftPillar;

        [SerializeField]
        public RectTransform rightPillar;

        [SerializeField]
        private float amount = 200;

        [SerializeField]
        private float jumpHeight = 50;

        [SerializeField]
        private RectTransform killRect;

        public TrailRenderer trailRenderer;

        public bool isJumping = false;

        public bool onLeftSide = true;
              
        private float leftPos;
        private float rightPos;
        private RectTransform myRect;

        [SerializeField]
        private RectTransform jumpPointRect;
        private bool inPowerUp;
        public Tween currentJumpTween;

        private PlayerHealthView healthView;

        [SerializeField]
        private DeathScreenView deathScreen;
        private AudioView audioView;

        //Get references.
        void Awake()
        {
            trailRenderer = GetComponent<TrailRenderer>();
            myRect = GetComponent<RectTransform>();
            myRect.localScale = Vector3.zero;
            healthView = GetComponent<PlayerHealthView>();
        }

        //Get Positions.
       protected override void Start()
        {
            base.Start();
            audioView = GetComponent<AudioView>();
            leftPos = leftPillar.position.x;
            rightPos = rightPillar.position.x;
            myRect.DOScale(1, 0.5f).SetEase(Ease.InOutExpo);
        }

        void Update()
        {
            if (!GameSettings.GameStarted) return;
            if (inPowerUp) return;
            if (healthView.hasDied)
            {
                if (currentJumpTween != null) currentJumpTween.Kill();
                return;
            }

            //Slide character down if not jumping.
            if (!isJumping)
            {
                trailRenderer.enabled = false;
                myRect.DOLocalMoveY(myRect.localPosition.y - 10, 0.1f);
                if (myRect.position.y < killRect.position.y)
                    deathScreen.ShowDeathScreen();
            }
        }

        //Jump the character with power.
        private void Jump(float direction, float power)
        {
            trailRenderer.enabled = true;
            isJumping = true;
            onLeftSide = !onLeftSide;
            audioView.PlayClip();
            float duration = 0.75f;
            myRect.DOScaleX(0.75f, duration*0.5f);
            //Animate the jump.
            currentJumpTween = transform.DOJump(new Vector3(direction, transform.position.y + (power *2.5f), 0), /*power*/5, 1, duration).OnComplete(() => 
            {
                isJumping = false;
         
                myRect.localScale = new Vector3(1, 1, 1);
            });
        }

        public void MoveWalls()
        {

        }

        //Stop all movement - rocket pockup.
        public void StopMoving()
        {
            inPowerUp = true;
            if (currentJumpTween != null)
            {
                currentJumpTween.Kill();
            }
        }

        //Continue movement
        public void Resume()
        {
            inPowerUp = false;
        }

        //Jump input heard.
        public void JumpPressed(float power)
        {
            if (!isJumping)
            {
                dispatcher.Dispatch(new InputEvent(InputEvent.Type.MOVE_WALLS));
                Jump(onLeftSide ? rightPos : leftPos, power);
            }
        }
    }
}

