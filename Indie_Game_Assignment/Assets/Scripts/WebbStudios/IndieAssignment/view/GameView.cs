﻿/*
 *Game view,
 * Toggles characters based on which was selected. *
 */
using UnityEngine;
using System.Collections;

namespace webbstudios.indieassignment.view
{
    public class GameView : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] characters;

        [SerializeField]
        private CameraFollow camFollow;

        public void Init(Characters character)
        {
            GameObject _character = characters[0];
            //Getting correct display.
            switch (character)
            {
                case Characters.POP:
                   _character = characters[0];
                    break;

                case Characters.WIFEY_POP:
                    _character = characters[1];
                    break;

                case Characters.GRAMPA_POP:
                    _character = characters[2];
                    break;

                case Characters.PUMPKIN_POP:
                    _character = characters[3];
                    break;

                case Characters.SANTA_POP:
                    _character = characters[4];
                    break;

            }

            _character.SetActive(true);
            camFollow.SetTarget(_character.transform);
        }
    }

}
