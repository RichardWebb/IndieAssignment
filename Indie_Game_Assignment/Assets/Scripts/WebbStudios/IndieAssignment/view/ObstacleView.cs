﻿/*
 * Obstacle view, 
 * Handles obstacles on the walls. 
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using webbstudios.indieassignment.constant;

namespace webbstudios.indieassignment.view
{
    public class ObstacleView : MonoBehaviour
    {
        [SerializeField]
        private bool shouldRotate;

        [SerializeField]
        private GameObject explosion;

        private bool active = true;
        private ObstacleSpawnerView view;

        //Start obstacle.
        public void Init(ObstacleSpawnerView parent) 
        {
            active = true;
            view = parent;
        }

        void Update()
        {
            if (active)
            {
                //Rotate if required.
                if (shouldRotate)
                    transform.Rotate(new Vector3(0, 0, +5f));

                //Check position - destroy if needed.
                if (transform.localPosition.y + 1000f < Camera.main.transform.localPosition.y )
                {
                    active = false;
                    KillObj();
                }
            }
        }
        //Hit from rocket - destroy me.
        public void DestroyFromHit()
        {
            active = false;
            GetComponent<CanvasGroup>().alpha = 0;
            GameObject obj = (GameObject)Instantiate(explosion, transform.position, Quaternion.Euler(Vector3.zero));
            Destroy(obj, 1f);

            view.FreeSpawnPoint();
            this.gameObject.Recycle();
        }

        //Clear obstacle.
        private void KillObj()
        {
            GetComponent<CanvasGroup>().alpha = 0;
            view.FreeSpawnPoint();
            this.gameObject.Recycle();
           // Destroy(this.gameObject);
        }
    }
}

