﻿/*
 * Power up manager - Toggles power ups when the are chosen.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;

namespace webbstudios.indieassignment.view
{
    public class PowerUpManagerView : EventView
    {
        [SerializeField]
        private PlayerMovementView moveMentView;

        [SerializeField]
        private JumpIndicatorView jumpIndicator;

        [SerializeField]
        private HelmetView helmetView;

        [SerializeField]
        private RocketView rocketView;

        [SerializeField]
        private DoubleScoreView doubleScoreView;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
        }

        //Check collision with power ups.
        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.tag == "PowerUp")
            {
                PowerUpView powerUp = col.gameObject.GetComponent<PowerUpView>();

                //Decide which power up and toggle on.
                switch (powerUp.myPowerUp)
                {
                    case PowerUpView.PowerUp.JUMP_AID:
                        //jumpIndicator.Activate();
                        break;

                    case PowerUpView.PowerUp.ROCKET:
                        rocketView.Activate();
                        break;

                    case PowerUpView.PowerUp.HELMET:
                        helmetView.Activate();
                        break;

                    case PowerUpView.PowerUp.TIMES_2_MODIFER:
                        doubleScoreView.Activate();
                        break;
                }

                //Remove display.
                Destroy(col.gameObject);
            }
        }
    }
}