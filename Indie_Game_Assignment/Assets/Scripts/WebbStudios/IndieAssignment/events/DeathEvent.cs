﻿/*
 * Death event - this event is dispatch to indicate the player has died.
 * Created by Richard Webb, s1308033 
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Extensions.EventManagement.API;
using System;

namespace webbstudios.indieassignment.events
{
    public class DeathEvent : IEvent
    {
        public enum Type
        {
            PLAYER_DIED
        }

        protected DeathEvent.Type _type;

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        public DeathEvent(DeathEvent.Type type)
        {
            _type = type;
        }
    }
}