﻿/*
 * Score event - dispatched to update scores from jumping / power up.
 * * Created by Richard Webb, s1308033  
  */

using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Extensions.EventManagement.API;
using System;

namespace webbstudios.indieassignment.events
{
    public class ScoreEvent : IEvent
    {
        public enum Type
        {
            UPDATE_SCORE
        }

        protected ScoreEvent.Type _type;
        protected int _score;

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        public int ScoreValue
        {
            get
            {
                return _score;
            }
        }

        public ScoreEvent(ScoreEvent.Type type, int scoreValue)
        {
            _type = type;
            _score = scoreValue;
        }
    }
}
