﻿/*
 * Input event - dispatched to indicate user inputs.
 * * Created by Richard Webb, s1308033  
  */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Extensions.EventManagement.API;
using System;

namespace WebbStudios.IndieAssignment.events
{
    public class InputEvent : IEvent
    {
        public enum Type
        {
            JUMP,
            MOVE_WALLS
        }

        protected InputEvent.Type _type;
        protected float _speed;

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        public float JumpSpeed
        {
            get
            {
                return _speed;
            }
        }

        public InputEvent(InputEvent.Type type)
        {
            _type = type;
        }


        public InputEvent(InputEvent.Type type, float speed): this(type)
        {
            _speed = speed;
        }
    }
}
