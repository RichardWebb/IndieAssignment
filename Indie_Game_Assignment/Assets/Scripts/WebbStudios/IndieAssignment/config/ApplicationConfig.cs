﻿/*
Application config.
This class hooks robotlegs's views, mediators and events.
 */

using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Framework.API;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventCommand.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Rancon.Extensions.BootstrapSettings.API;
using System;
using webbstudios.indieassignment.events;
using webbstudios.indieassignment.view;
using webbstudios.indieassignment.view.interfaces;
using webbstudios.indieassignment.mediator;

namespace webbstudios.indieassignment.config
{
    public class ApplicationConfig : IConfig
    {
        [Inject]
        public IContext context;

        [Inject]
        public IMediatorMap mediatorMap;

        [Inject]
        public IInjector injector;

        [Inject]
        public IEventCommandMap commandMap;

        [Inject]
        public IEventDispatcher dispatcher;

        //hook up mappings.
        public void Configure()
        {
            mediatorMap.Map<IInputListener>().ToMediator<InputMediator>();
            mediatorMap.Map<IScoreDispatcher>().ToMediator<ScoreDispatcherMediator>();
            mediatorMap.Map<IScoreListener>().ToMediator<ScoreListnerMediator>();
            mediatorMap.Map<IDeathListener>().ToMediator<DeathMediator>();
            context.AfterInitializing(PostInit);
        }

        private void PostInit()
        {
        }
    }
}
