﻿/*
 * ScoreListnerMediator - mediator listens for score events and sends the event to the listening view.
  * Created by Richard Webb, s1308033  
 */

using UnityEngine;
using System.Collections;
using webbstudios.indieassignment.view.interfaces;
using Robotlegs.Bender.Bundles.MVCS;
using webbstudios.indieassignment.events;
using System;

namespace webbstudios.indieassignment.mediator
{
    public class ScoreListnerMediator : Mediator
    {
        [Inject]
        public IScoreListener view;

        public override void Initialize()
        {
            base.Initialize();
            AddContextListener<ScoreEvent>(ScoreEvent.Type.UPDATE_SCORE, UpdateScore);
        }

        private void UpdateScore(ScoreEvent obj)
        {
            view.UpdateScore(obj.ScoreValue);
        }
    }
}

