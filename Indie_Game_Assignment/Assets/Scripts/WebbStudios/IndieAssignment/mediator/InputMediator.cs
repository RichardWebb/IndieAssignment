﻿/*
 * InputMediator - mediator listens for input events and dispatchs the message to all input listeners.
  * Created by Richard Webb, s1308033  
 */

using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Bundles.MVCS;
using WebbStudios.IndieAssignment.events;
using webbstudios.indieassignment.view.interfaces;
using System;

namespace webbstudios.indieassignment.mediator
{
    public class InputMediator : Mediator
    {
        [Inject]
        public IInputListener view;

        public override void Initialize()
        {
            base.Initialize();

            AddContextListener<InputEvent>(InputEvent.Type.JUMP, HandleJump);
            AddContextListener(InputEvent.Type.MOVE_WALLS, view.MoveWalls);
            AddViewListener(InputEvent.Type.JUMP, Dispatch);
            AddViewListener(InputEvent.Type.MOVE_WALLS, Dispatch);
        }

        private void HandleJump(InputEvent obj)
        {
            view.JumpPressed(obj.JumpSpeed);
        }
    }   
}