﻿/*
 * ScoreListnerMediator - mediator listens for score events and dispatchs them to the listners.
  * Created by Richard Webb, s1308033  
 */

using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Bundles.MVCS;
using webbstudios.indieassignment.view.interfaces;
using webbstudios.indieassignment.events;

namespace webbstudios.indieassignment.mediator
{ 
    public class ScoreDispatcherMediator : Mediator
    {
        [Inject]
        public IScoreDispatcher view;

        public override void Initialize()
        {
            base.Initialize();
            AddViewListener(ScoreEvent.Type.UPDATE_SCORE, Dispatch);
        }
    }
}
