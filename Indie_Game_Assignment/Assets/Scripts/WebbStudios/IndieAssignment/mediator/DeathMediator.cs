﻿/*
 * DeathMediator - mediator listens for players dead and dispatchs message to the listeners.
  * Created by Richard Webb, s1308033  
 */


using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Bundles.MVCS;
using webbstudios.indieassignment.events;
using webbstudios.indieassignment.view.interfaces;

namespace webbstudios.indieassignment.mediator
{ 
    public class DeathMediator : Mediator
    {
        [Inject]
        public IDeathListener view;

        public override void Initialize()
        {
            base.Initialize();
            AddContextListener(DeathEvent.Type.PLAYER_DIED, view.PlayerDied);
            AddViewListener(DeathEvent.Type.PLAYER_DIED, Dispatch);
        }
    }  
}
