﻿namespace Rancon.Extensions.BootstrapSettings.API
{
    public interface IBootstrapSettingsRequester
    {
        void GotBootstrapSetting(string key, object value);
    }
}
