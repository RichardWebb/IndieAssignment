﻿using System.Runtime.Serialization;
namespace Rancon.Extensions.BootstrapSettings.API
{
    [DataContract(Name="version", Namespace = "")]
    public class AppVersion
    {
        [DataMember(Name="major")]
        public int Major { get; set; }

        [DataMember(Name = "minor")]
        public int Minor { get; set; }

        [DataMember(Name = "patch")]
        public int Patch { get; set; }

        [DataMember(Name = "build")]
        public int Build { get; set; }

        public override string ToString()
        {
            return Major+"."+Minor+"."+Patch+" Build:"+Build;
        }
    }
}
