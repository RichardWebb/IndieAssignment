using Rancon.Extensions.BootstrapSettings.API;
using Rancon.Extensions.BootstrapSettings.Impl;
using Robotlegs.Bender.Extensions.Mediation.API;
using System;

namespace Rancon.Extensions.BootstrapSettings
{
	public class BootstrapSettingsConfig : IBootstrapSettingsConfig
	{
        public string BootstrapSettingsPath { get; private set; }
        public bool LoadAsync { get; private set; }
        public Type ModelType { get; private set; }
        public int TimerInterval { get; private set; }
        public Action BeforeLoad { get; private set; }
        public Action AfterLoad { get; private set; }
        public Action<int> OnLoadProgress { get; private set; }
        public Action OnLoadFail { get; private set; }
        public bool Softly { get; private set; }

		public BootstrapSettingsConfig () : this("bootstrap-settings.xml") {}
		
		public BootstrapSettingsConfig (string bootstrapSettingsPath) : this(bootstrapSettingsPath, true) {}

        public BootstrapSettingsConfig(string bootstrapSettingsPath, bool loadAsync) : this(bootstrapSettingsPath, loadAsync, typeof(XMLBootstrapSettingsModel)) { }

        public BootstrapSettingsConfig(string bootstrapSettingsPath, Type modelType) : this(bootstrapSettingsPath, true, modelType) { }

        public BootstrapSettingsConfig(string bootstrapSettingsPath, bool loadAsync, Type modelType)
        {
            this.BootstrapSettingsPath = bootstrapSettingsPath;
            this.LoadAsync = loadAsync;
            this.ModelType = modelType;
            WithTimerInterval(100);
        }

        public IBootstrapSettingsConfig WithPath(string path)
        {
            this.BootstrapSettingsPath = path;
            return this;
        }

        public IBootstrapSettingsConfig WithTimerInterval(int timerInterval)
        {
            TimerInterval = timerInterval;
            return this;
        }

        //TODO: Implement 'WithTimeout' into code
        /*
        public IBootstrapSettingsConfig WithTimeout(int timeout)
        {
            Timeout = timeout;
            return this;
        }
        */

        public IBootstrapSettingsConfig WithBeforeLoad(Action beforeLoad)
        {
            BeforeLoad = beforeLoad;
            return this;
        }

        public IBootstrapSettingsConfig WithAfterLoad(Action afterLoad)
        {
            AfterLoad = afterLoad;
            return this;
        }

        public IBootstrapSettingsConfig WithOnLoadFail(Action onLoadFail)
        {
            OnLoadFail = onLoadFail;
            return this;
        }
        
        public IBootstrapSettingsConfig WithOnLoadProgress(Action<int> onLoadProgress)
        {
            OnLoadProgress = onLoadProgress;
            return this;
        }

        public IBootstrapSettingsConfig Asynchronously()
        {
            this.LoadAsync = true;
            return this;
        }

        public IBootstrapSettingsConfig Synchronously()
        {
            this.LoadAsync = false;
            return this;
        }

        public IBootstrapSettingsConfig LoadMeSoftlyWithThisSong()
        {
            Softly = true;
            return this;
        }

        public IBootstrapSettingsConfig NotSoftly()
        {
            Softly = false;
            return this;
        }

        [Inject]
        public IMediatorMap mediatorMap;

        public void Configure()
        {
            mediatorMap.Map(typeof(IBootstrapSettingsRequester)).ToMediator(typeof(BootstrapSettingsRequesterMediator));
        }
    }
}

