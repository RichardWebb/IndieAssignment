﻿
using System;

namespace Rancon.Extensions.BootstrapSettings.Impl
{
    public class BootstrapSettingsException : Exception
    {
        
        public BootstrapSettingsException()
        {

        }

        public BootstrapSettingsException(string message) : base (message)
        {

        }
    }
}
