﻿using Rancon.Extensions.BootstrapSettings.API;
using Rancon.Extensions.CallbackTimer.API;
using Robotlegs.Bender.Framework.API;
using System;
using System.Linq;
using System.Collections;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Rancon.Extensions.BootstrapSettings.Impl
{
    public class XMLBootstrapSettingsModel : IBootstrapSettingsModel
    {
        [Inject]
        public IInjector injector;

        public bool LoadCompleted { get; private set; }
        
        public Action BeforeLoad { get; set; }

        public Action AfterLoad { get; set; }

        public Action<int> OnLoadProgress { get; set; }

        public Action OnLoadFail { get; set; }

        private XmlDocument BootstrapSettingsXml;

#if UNITY_ANDROID
        UnityEngine.WWW www;
#endif
        public ICallbackTimer callbackTimer;
        private Action<bool> loadCallback;
        private bool loadFailed;
        private int currentProgress;
        private long loadedByteCount;

        public bool LoadBootstrapSettings(string bootstrapSettingsPath)
        {
            LoadCompleted = false;
            if (BeforeLoad != null) BeforeLoad();
            BootstrapSettingsXml = new XmlDocument();
            
            try
            {
                BootstrapSettingsXml = new XmlDocument();
                BootstrapSettingsXml.Load(bootstrapSettingsPath);
            }
            catch
            {
                if (OnLoadFail != null) OnLoadFail();
                return false;
            }
            LoadCompleted = true;
            if (AfterLoad != null) AfterLoad();
            return BootstrapSettingsXml != null;
        }



        public bool LoadBootstrapSettingsAsync(string bootstrapSettingsPath)
        {
            return LoadBootstrapSettingsAsync(bootstrapSettingsPath, null);
        }

        public bool LoadBootstrapSettingsAsync(string bootstrapSettingsPath, Action<bool> loadCallback)
        {
            return LoadBootstrapSettingsAsync(bootstrapSettingsPath, loadCallback, 100);
        }

        public bool LoadBootstrapSettingsAsync(string bootstrapSettingsPath, Action<bool> loadCallback, int timerInterval)
        {
            if (!injector.HasDirectMapping(typeof(ICallbackTimer)))
            {
                //TODO: How to handle when the dependency is not met?
                throw (new Exception("No ICallbackTimer installed. The callback timer extension is a dependency when using the async features of the BootstrapSettings extension."));
            }
            LoadCompleted = false;
            this.loadCallback = loadCallback;
            if (BeforeLoad != null) BeforeLoad();

#if UNITY_ANDROID
            www = new UnityEngine.WWW(bootstrapSettingsPath);
#else
            WebClient webClient = new WebClient();
            webClient.DownloadDataCompleted += WebClientComplete;
            webClient.DownloadProgressChanged += WebClientProgress;

            Uri uri;
            try
            {
                uri = new Uri(bootstrapSettingsPath);
            }
            catch
            {
                LoadCompleted = true;
                if (OnLoadFail != null) OnLoadFail();
                return false;
            }
			webClient.DownloadDataAsync(uri);
#endif
            callbackTimer = injector.GetInstance<ICallbackTimer>();
			if(timerInterval == 1)
			{
				callbackTimer.StartFrameTimer(timerInterval, CheckForBootstrapLoadComplete);
			}
			else
			{
            	callbackTimer.StartTimer(timerInterval, CheckForBootstrapLoadComplete);
			}
            return true;
        }

        private void WebClientProgress(object sender, DownloadProgressChangedEventArgs e)
        {
            loadedByteCount += e.BytesReceived;
            currentProgress = (int)((double)loadedByteCount / e.TotalBytesToReceive * 100);
        }

        private void WebClientComplete (object sender, DownloadDataCompletedEventArgs e)
        {
            if (e.Cancelled || e.Error != null)
            {
                loadFailed = true;
            }
            else
            {
                BootstrapSettingsXml = new XmlDocument();
                try
                {
                    BootstrapSettingsXml.LoadXml(Encoding.UTF8.GetString(e.Result));
                }
                catch
                {
                    loadFailed = true;
                }
            }
            WebClient webClient = (WebClient) sender;
            webClient.DownloadProgressChanged -= WebClientProgress;
            webClient.DownloadDataCompleted -= WebClientComplete;
            webClient.Dispose();
        }

        private void CheckForBootstrapLoadComplete()
        {
#if UNITY_ANDROID
            if (OnLoadProgress != null) OnLoadProgress((int) (www.progress*100));
            if (www.isDone)
            {
                if(www.error != null)
                {
                    loadFailed = true;
                    UnityEngine.Debug.Log(www.error);
                }
                else
                {
                    BootstrapSettingsXml = new XmlDocument();
                    BootstrapSettingsXml.LoadXml(www.text);
                }
                BootstrapLoadComplete();
            }
#else
            if (OnLoadProgress != null) OnLoadProgress(currentProgress);
            if (loadFailed || BootstrapSettingsXml != null)
            {
                BootstrapLoadComplete();
            }
#endif
        }
        private void BootstrapLoadComplete()
        {
            callbackTimer.Stop();
            callbackTimer.Dispose();
            callbackTimer = null;
            LoadCompleted = true;
            if (loadFailed && OnLoadFail != null) OnLoadFail();
            else if (!loadFailed && AfterLoad != null) AfterLoad();
            if (loadCallback != null)
                loadCallback(BootstrapSettingsXml != null);
        }

        public bool HasSetting(string key)
        {
            return GetSetting(typeof(object), key) != null;
        }

        public T GetSetting<T>()
        {
            return (T)GetSetting(typeof(T));
        }

        public T GetSetting<T>(string key)
        {
            return (T)GetSetting(typeof(T), key);
        }

        public object GetSetting(string key)
        {
            return GetSetting(typeof(object), key);
        }
        
        public object GetSetting(Type type)
        {
            return GetSetting(type, "");
        }
        public object GetSetting(Type type, string key)
        {
            if (BootstrapSettingsXml == null)
                return null;

            XmlNode node;
            object returnObj = null;

            if (key == "" && Attribute.IsDefined(type, typeof(DataContractAttribute)))
            {
                object[] attributes = type.GetCustomAttributes(typeof(DataContractAttribute), false);
                key = "//" + ((DataContractAttribute)attributes.Single(attribute => attribute is DataContractAttribute)).Name;
            }
            node = BootstrapSettingsXml.SelectSingleNode(key);

            if (node != null)
            {
                if(type == typeof(bool))
                {
                    returnObj = bool.Parse(node.InnerText);
                }
                else if (Attribute.IsDefined(type, typeof(DataContractAttribute)))
                {
                    try
                    {
                        DataContractSerializer dataSerializer = new DataContractSerializer(type);
                        returnObj = dataSerializer.ReadObject(new XmlNodeReader(node));
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.Log(e);
                    }
                }
                
                if(returnObj == null)
                {
                    returnObj = type == typeof(XmlNode) ? node : (object) node.InnerText;
                }
            }
            return returnObj;
        }
    }
}
