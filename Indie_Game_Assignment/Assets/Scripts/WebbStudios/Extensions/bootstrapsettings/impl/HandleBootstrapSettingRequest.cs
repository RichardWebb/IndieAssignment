﻿using Rancon.Extensions.BootstrapSettings.API;
using Robotlegs.Bender.Extensions.CommandCenter.API;
using Robotlegs.Bender.Extensions.EventManagement.API;

namespace Rancon.Extensions.BootstrapSettings.Impl
{
    public class HandleBootstrapSettingRequest : ICommand
    {
        [Inject]
        public BootstrapSettingsEvent evt;

        [Inject]
        public IBootstrapSettingsModel model;

        [Inject]
        public IEventDispatcher dispatcher;

        public void Execute()
        {
            switch((BootstrapSettingsEvent.Type) evt.type)
            {
                case BootstrapSettingsEvent.Type.GET_SETTING:
                    dispatcher.Dispatch(new BootstrapSettingsEvent(BootstrapSettingsEvent.Type.GOT_SETTING, evt.Key, evt.ReturnType, model.GetSetting(evt.ReturnType, evt.Key)));
                    break;
            }
        }
    }
}
