using Robotlegs.Bender.Framework.API;
using Robotlegs.Bender.Extensions.Matching;
using Rancon.Extensions.BootstrapSettings.API;
using System;
using Robotlegs.Bender.Extensions.EventCommand.API;
using Rancon.Extensions.BootstrapSettings.Impl;

namespace Rancon.Extensions.BootstrapSettings
{
    public class BootstrapSettingsExtension : IExtension
    {
        private IContext context;
        private IBootstrapSettingsConfig bootstrapSettingsConfig;

        public void Extend(IContext context)
        {
            this.context = context;
            context.AddConfigHandler(new InstanceOfMatcher(typeof(IBootstrapSettingsConfig)), BootstrapSettingsConfigured);
            context.BeforeInitializing(BeforeContextInitalizing);
        }

        private void BootstrapSettingsConfigured(object bootstrapSettingsConfigObj)
        {
            bootstrapSettingsConfig = (IBootstrapSettingsConfig) bootstrapSettingsConfigObj;
        }

        private void BeforeContextInitalizing(object message, HandlerAsyncCallback callback)
        {
            if (bootstrapSettingsConfig == null)
            {
                context.Configure(new BootstrapSettingsConfig());
            }

            IEventCommandMap eventCommandMap = context.injector.GetInstance<IEventCommandMap>();
            if (eventCommandMap != null)
            {
                eventCommandMap.Map(BootstrapSettingsEvent.Type.GET_SETTING).ToCommand(typeof(HandleBootstrapSettingRequest));
            }

            IBootstrapSettingsModel bootstrapSettingsModel = (IBootstrapSettingsModel) Activator.CreateInstance(bootstrapSettingsConfig.ModelType);
            context.injector.InjectInto(bootstrapSettingsModel);
            context.injector.Map(typeof(IBootstrapSettingsModel)).ToValue(bootstrapSettingsModel);
			

            if (bootstrapSettingsConfig.BeforeLoad != null)
            {
                bootstrapSettingsModel.BeforeLoad += bootstrapSettingsConfig.BeforeLoad;
            }
            if (bootstrapSettingsConfig.AfterLoad != null)
            {
                bootstrapSettingsModel.AfterLoad += bootstrapSettingsConfig.AfterLoad;
            }
            if (bootstrapSettingsConfig.OnLoadFail != null)
            {
                bootstrapSettingsModel.OnLoadFail += bootstrapSettingsConfig.OnLoadFail;
            }
            if (bootstrapSettingsConfig.OnLoadProgress != null)
            {
                bootstrapSettingsModel.OnLoadProgress += bootstrapSettingsConfig.OnLoadProgress;
            }

            if (bootstrapSettingsConfig.LoadAsync)
            {
                if (!bootstrapSettingsModel.LoadBootstrapSettingsAsync(bootstrapSettingsConfig.BootstrapSettingsPath, (result) => { HandleLoadComplete(result, callback, bootstrapSettingsModel); }, bootstrapSettingsConfig.TimerInterval))
                {
                    HandleLoadComplete(false, callback, bootstrapSettingsModel);
                }
            }
            else
            {
                HandleLoadComplete(
                    bootstrapSettingsModel.LoadBootstrapSettings(bootstrapSettingsConfig.BootstrapSettingsPath),
                    callback, bootstrapSettingsModel);
            }
        }
        
        private void HandleLoadComplete(bool result, HandlerAsyncCallback callback, IBootstrapSettingsModel bootstrapSettingsModel)
        {
            if (result)
            {
                if (callback != null)
                {
                    callback();
                }
            }
            else
            {
                if (bootstrapSettingsConfig.Softly)
                {
                    ILogging logger = context.GetLogger(this);
                    if (logger != null) logger.Info("Bootstrap Settings failed to load. Continuing as soft load is enabled: " + bootstrapSettingsConfig.BootstrapSettingsPath);
                    callback();
                }
                else
                {
                    callback(new BootstrapSettingsException("Bootstrap Settings failed to load file errored : " + bootstrapSettingsConfig.BootstrapSettingsPath));
                }
            }
        }
    }
}