﻿using Rancon.Extensions.CallbackTimer.API;
using Rancon.Extensions.CallbackTimer.Impl;
using Robotlegs.Bender.DependencyProviders;
using Robotlegs.Bender.Framework.API;

namespace Rancon.Extensions.CallbackTimer
{
    public class CallbackTimerExtension : IExtension
    {
        public void Extend(IContext context)
        {
			context.injector.Map (typeof(ICallbackTimer)).ToSingleton<CallbackTimer.Impl.CallbackTimer> (true);
        }
    }
}
